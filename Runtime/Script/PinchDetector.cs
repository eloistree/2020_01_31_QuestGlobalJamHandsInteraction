﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PinchDetector : MonoBehaviour
{
    public Transform m_fingerTipA;
    public Transform m_fingerTipB;
    public float m_distanceToPinch=0.05f;

    public PinchingChangeEvent m_onPinchChange;
    [Header("Debug")]
    public bool m_isPinching;

    private bool m_previousPinching;
   
    void Update()
    {
        if (m_previousPinching != m_isPinching) {
            m_onPinchChange.Invoke(m_isPinching);
            m_previousPinching = m_isPinching;
        }
        m_isPinching = Vector3.Distance(m_fingerTipA.position, m_fingerTipB.position) < m_distanceToPinch;
    }

    public Quaternion GetPinchRotation()
    {
        return Quaternion.Slerp(m_fingerTipA.rotation, m_fingerTipB.rotation, 0.5f);
    }

    public Vector3 GetPinchPosition()
    {
        return (m_fingerTipA.position + m_fingerTipB.position) / 2f;
    }

    [System.Serializable]
    public class PinchingChangeEvent : UnityEvent<bool> { }

}

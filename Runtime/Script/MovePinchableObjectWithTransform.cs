﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePinchableObjectWithTransform : MonoBehaviour
{
    public PinchDetector m_pinchDetector;
    public Transform m_oriantation;
    public LayerMask m_affectedLayers = -1;
    [Header("Debug")]
    public Quaternion m_rotationDifferenceAtPinch;
    public PinchableObject m_objectsToMove;


    public void Start()
    {
        m_pinchDetector.m_onPinchChange.AddListener(Pinching);
    }
    public void Update()
    {

        if (m_objectsToMove) {

            m_objectsToMove.transform.position = m_pinchDetector.GetPinchPosition();
            m_objectsToMove.transform.rotation = m_oriantation.rotation * m_rotationDifferenceAtPinch  ;
        }
        
    }

    public void Pinching(bool state)
    {

        if (state)
            StartPinching();
        else StopPinching();

    }
    
    void StartPinching()
    {
        m_objectsToMove = GetPinchableObjectInRegion();
        if (m_objectsToMove != null) {
            m_objectsToMove.NotifyAsStartPinching();
            m_rotationDifferenceAtPinch = Quaternion.Inverse(m_oriantation.rotation)* m_objectsToMove.transform.rotation ;
        }
        else m_rotationDifferenceAtPinch = Quaternion.identity;
        //for (int i = 0; i < m_objectsToMove.Count; i++)
        //{
        //    m_objectsToMove[i].StopUsingRigidbodyIfPresent();
        //}

    }

    private PinchableObject GetPinchableObjectInRegion()
    {
        Collider[] touched = Physics.OverlapSphere(m_pinchDetector.GetPinchPosition(), 0.01f, m_affectedLayers);
        for (int i = 0; i < touched.Length; i++) {
            PinchableObject pinchable = touched[i].GetComponent<PinchableObject>();

            if (pinchable != null)
                return pinchable;
        }
        return null;

    }
    
    void StopPinching()
    {

        if (m_objectsToMove != null) {

            m_objectsToMove.ResetVelocity();
            m_objectsToMove.NotifyAsReleasing();
            m_objectsToMove = null;
        }
        

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PinchableObject : MonoBehaviour
{
    public UnityEvent m_onStartPinch;
    public UnityEvent m_onRelease;

  
    public void ResetVelocity() {
        Rigidbody rig = GetComponent<Rigidbody>();
        if (rig) {
        rig.velocity = Vector3.zero;
        rig.angularVelocity = Vector3.zero;
        }
    }

    public void NotifyAsStartPinching()
    {
        m_onStartPinch.Invoke();
    }

    public void NotifyAsReleasing()
    {
        m_onRelease.Invoke();
    }
}
